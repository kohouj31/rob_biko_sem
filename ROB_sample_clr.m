siz = size(table);
R = 0 ;
B = 0 ;
G = 0;
for i = 1:siz(1)
    [R_t, G_t, B_t] = sample_color(table(floor(i),2), table(floor(i),3),img);
    R = R + R_t;
    G = G + G_t;
    B = B + B_t;
end

R = R/siz(1);
G = G/siz(1);
B = B/siz(1);

function [R, G, B] = sample_color(x,y,img)
    R = img(floor(y)-5:floor(y)+5,floor(x)-5:floor(x)+5, 1);
    G = img(floor(y)-5:floor(y)+5,floor(x)-5:floor(x)+5, 2);
    B = img(floor(y)-5:floor(y)+5,floor(x)-5:floor(x)+5, 3);
    R = sum(R,'all')/121;
    G = sum(G,'all')/121;
    B = sum(B,'all')/121;
end

