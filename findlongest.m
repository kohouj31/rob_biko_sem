function [maxl maxp] = findlongest(extrems)
maxl = 0;
maxp= nan;
extrems = [extrems; extrems(1,:)]
for l= 1:8
    tmp = extrems(l+1,:)-extrems(l,:);
    lgth = vecnorm(tmp)
    if lgth > maxl
        maxl = lgth;
        maxp = [extrems(l+1,:); extrems(l,:)];
    end
end
end