function R=ROB_collision_check(cube, all_cubes)
    alpha = (cube(1,4)/180)*pi;
    unit = 13.85;
    [h,~]=size(all_cubes);
    Temp = zeros(h,2);
    %count distance between two cubes and angle
    function [dist, ang]=give_dist(cube1, cube2)
        vect = [-cube1(1)+cube2(1), -cube1(2)+cube2(2)];
        ang = atan2(vect(2),vect(1));
        dist = sqrt((cube1(1)-cube2(1))^2 + (cube1(2)-cube2(2))^2);
    end
    %distance of cubes from given cube
    for i = 1:h
        Temp(i,:) = give_dist(cube(1,2:3), all_cubes(i,2:3));
    end
    row = 1;
    
    %far cubes exclusion
    for i = 1:h
        if (Temp(i,1) <= (8 + sqrt(18))*unit)&&(Temp(i,1)>0)
            D(row,:) = all_cubes(i,:); %store "bad" cubes
            row = row + 1;
        end
    end
    if row > 1
        %check axis x direction
        x = 1;
        tp1 = [cube(1,2)+6*unit*sin(alpha),cube(1,3)+6*unit*cos(alpha)];
        tp2 = [cube(1,2)-6*unit*sin(alpha),cube(1,3)-6*unit*cos(alpha)];
        tp1_2 = [cube(1,2)+3*unit*sin(alpha),cube(1,3)+3*unit*cos(alpha)];
        tp2_2 = [cube(1,2)-3*unit*sin(alpha),cube(1,3)-3*unit*cos(alpha)];
        y = 1;
        tp3 = [cube(1,2)+6*unit*sin(alpha+pi/2),cube(1,3)+6*unit*cos(alpha+pi/2)];
        tp4 = [cube(1,2)-6*unit*sin(alpha+pi/2),cube(1,3)-6*unit*cos(alpha+pi/2)];
        tp3_2 = [cube(1,2)+3*unit*sin(alpha+pi/2),cube(1,3)+3*unit*cos(alpha+pi/2)];
        tp4_2 = [cube(1,2)-3*unit*sin(alpha+pi/2),cube(1,3)-3*unit*cos(alpha+pi/2)];



        for i = 1:(row-1)
            [d1, ~] = give_dist(tp1, D(i, 2:3));
            [d2, ~] = give_dist(tp1_2, D(i, 2:3));

            [d3, ~] = give_dist(tp2, D(i, 2:3));
            [d4, ~] = give_dist(tp2_2, D(i, 2:3));

            [d5, ~] = give_dist(tp3, D(i, 2:3));
            [d6, ~] = give_dist(tp3_2, D(i, 2:3));

            [d7, ~] = give_dist(tp4, D(i, 2:3));
            [d8, ~] = give_dist(tp4_2, D(i, 2:3));

            if (d1 < 4*unit)||(d2 < 6*unit)
                x = 0;
            end
            if (d3 < 4*unit)||(d4 < 6*unit)
                x = 0;
            end
            if (d5 < 4*unit)||(d6 < 6*unit)
                y = 0;
            end
            if (d7 < 4*unit)||(d8 < 6*unit)
                y = 0;
            end
        end

        if x == 1 && y == 1
            R = 3;
        elseif x == 1 && y == 0
            R = 1;
        elseif x == 0 && y == 1
            R = 2;
        else
            R = 0;
        end
    else
        R = 3;
    end
end