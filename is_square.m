%Jan Kohout 2023
function angle = is_square(points, center, area_detcted, img)
%https://softwareengineering.stackexchange.com/a/177687
side = sqrt(area_detcted);
p1 = center + side/2;
p2 = center - side/2;
p3 = [center(1)+side/2, center(2)-(side/2)];
p4 = [center(1)-side/2, center(2)+(side/2)];
tofit_points = [p1;p3;p2;p4];
best_value = nan;
best_angle = nan;
best_points = nan;
%detected polygon
detected = polyshape(points(:,1), points(:,2));
for iter_angel = -45:45
    %rotate the squre
    iter_angel = iter_angel;
    iter_angel_rad = deg2rad(iter_angel);
    tofit_points_rot = rotation_f(tofit_points, iter_angel_rad, center);
    %square created based on detected area
    tofit = polyshape(tofit_points_rot(:,1),tofit_points_rot(:,2));
    interse = intersect(tofit,detected);
    inter_area = area(interse);
    
    tofit = polyshape(tofit_points_rot(:,1),tofit_points_rot(:,2));
    plot(detected);
    hold on
    plot(tofit);
    plot(center(1),center(2),'o','MarkerFaceColor','yellow');
    hold off
    pause(1.5)
    
    
    if isnan(best_value) || inter_area > best_value
       best_points = tofit_points_rot;
       best_angle = iter_angel;
       best_value = inter_area;
    end
end



tofit = polyshape(best_points(:,1),best_points(:,2));
imshow(img);
hold on
plot(detected);

plot(p2(1),p2(2),'o','MarkerFaceColor','red');
plot(p4(1),p4(2),'o','MarkerFaceColor','red');
plot(tofit);
plot(center(1),center(2),'o','MarkerFaceColor','yellow');
hold off

angle = best_angle

end

function vector = rotation_f(vect,angle, center)
%transform to origin
vect = vect - center;
M = @(angle)[cos(angle) -sin(angle);...
     sin(angle)  cos(angle)];
%rotate
vector = (M(angle)*vect')';
%transfor back to center
vector = vector + center;
end