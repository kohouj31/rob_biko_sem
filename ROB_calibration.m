function H = ROB_calibration(pointtab)
% zelena 0.12
% cerna 0.1
% modra 0.37
% cervena 0.30
% oranzova 0.41

%pevne parametry kostek v ss robota a barvy pozic
r_p = [450 0 0.12;350 200 0.41;550 200 0.1; 550 -250 0.30;250 -300 0.20];
%zelena = 687.6, 590;
%cerna = 535.8, 867.1;
%modra = 978.2, 179.9;
%cervena = 552.4, 230.25;
%oranzova = 824.5, 869.3;

table = [978.2 179.9 250 -300;535.8 867.1 550 200;824.5, 869.3 350 200;552.4, 230.25 550 -250; 687.6 590 450 0]
points = zeros(5,4);

%sestaveni tabulky s x1 y1 x2 y2
 for a = 1:5
     points(a,3:4) = r_p(a,1:2);
     for b = 1:5
         pointtab(b,7)
         r_p(a,3)
         if pointtab(b,7) == r_p(a,3)
             points(a,1:2) = pointtab(b,(2:3));
         end
     end
 end




%sestaveni matice pro vypocet koeficientu h
for i = 1:5
    temp = t_A(points(i,1),points(i,2),points(i,3),points(i,4));
    if i == 1
        A = temp;
    else
        A = [A;temp];
    end
end

%vypocet koef h a reshape na 3x3 matici
[~,~,V] = svd(A);
H=V(:,end);
H=reshape(H,3,3);

projected_point=[475.16 558.08 1] * H;
t1 = projected_point(1,1)/projected_point(1,3);
t2 = projected_point(1,2)/projected_point(1,3);
t = [t1 t2];
function tA = t_A(x1,y1,x2,y2)
    tA = [-x1 -y1 -1 0 0 0 x1*x2 x2*y1 x2; 0 0 0 -x1 -y1 -1 y2*x1 y2*y1 y2];
end
end